Here you will find:

NodeXL 
- Crawled tweets in a timespan of three weeks, with over 150,000 tweets, all related to health rumors keywords.
- Last 10 tweets of uniques users in the first crawled dataset
- Predicted datasets
- Most relevant Visual Clustering Graphs generated
- Spreadsheets of individual crawling sessions
