rate_status = None

class RateStatus:
    def __init__(self):
        self.limit = None
        self.remaining = None
        self.reset = None

    def extract(self, tweepy_rate):
        self.limit = tweepy_rate['resources']['statuses']['/statuses/user_timeline']['limit']
        self.remaining = tweepy_rate['resources']['statuses']['/statuses/user_timeline']['remaining']
        self.reset = tweepy_rate['resources']['statuses']['/statuses/user_timeline']['reset']

    def decrement_remaining(self):
        self.remaining = self.remaining - 1

def get_rate_status(api, refresh=False):
    global rate_status

    if refresh is True or rate_status is None:
        rate_status = RateStatus()

    rate_status.extract(api.rate_limit_status())
    return rate_status