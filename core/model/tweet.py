import json
from author import Author

class Tweet:
    def __init__(self):
        self.id = None # id
        self.text = None # text
        self.normalizedText = None  # text
        self.favorite_count = None #favorite_count
        self.created_at = None # created_at
        self.lang = None # lang
        self.retweet_count = None #retweet_count
        self.author = Author()
        self.health = None
        self.rumor = None
        self.rumorProb = None
    def extract_from_tweepy(self, tweepy_tweet):
        self.id = tweepy_tweet.id
        self.text = tweepy_tweet.text.encode('UTF-8')
        self.favorite_count = tweepy_tweet.favorite_count
        self.retweet_count = tweepy_tweet.retweet_count
        self.lang = tweepy_tweet.lang.encode('UTF-8')
        self.created_at = tweepy_tweet.created_at
        self.author.extract(tweepy_tweet.author)
    def extract_from_csv(self, csv_tweet):
        self.id = csv_tweet[0]
        self.text = csv_tweet[1]
        self.normalizedText = csv_tweet[2]
        self.favorite_count = csv_tweet[3]
        self.retweet_count = csv_tweet[4]
        self.lang = csv_tweet[5]
        self.created_at = csv_tweet[6]
        self.author.id = csv_tweet[7]
        self.health = csv_tweet[8]
        self.rumor = csv_tweet[9]
        self.rumorProb = float(csv_tweet[10])
    def extract_from_db(self, db_tweet):
        self.id = db_tweet[1]
        self.author.id = db_tweet[2]
        self.text = db_tweet[4] if db_tweet[3] is None else db_tweet[3]
        self.favorite_count = db_tweet[5]
        self.retweet_count = db_tweet[6]
        self.lang = db_tweet[7]
        self.created_at = db_tweet[8]
        self.health = db_tweet[9]
        self.rumor = db_tweet[10]
        pass
    def items(self):
        return [self.id,
                self.text,
                self.normalizedText,
                self.favorite_count,
                self.retweet_count,
                self.lang,
                self.created_at,
                self.author.id,
                int(self.health),
                self.rumor,
                self.rumorProb]
    def __str__(self):
        return '{},{},{},{},{},{},{}'.format(self.id, self.text, self.favorite_count, \
                                                self.retweet_count, self.lang, self.created_at, self.author.id)

    def to_json(self):
        return {
            'id': self.id,
            'text': self.text,
            'normalizedText': self.normalizedText,
            'health': int(self.health),
            'rumor': int(self.rumor),
            'rumorProbRaw': self.rumorProb,
            'rumorProb': str("{0:.2f}%".format(self.rumorProb*100))
        }

                 # Status(contributors=None, truncated=False, text=u"Medical News Today: 'Enhanced' environment boosts mouse immune systems: Breaking research demo... https://t.co/WyKB9E5Th0 #medical #news", is_quote_status=False, in_reply_to_status_id=None, id=782157108338839552, favorite_count=1, _api=<tweepy.api.API object at 0x7fcb41efb810>,
        # author=User(follow_request_sent=False, has_extended_profile=False, profile_use_background_image=True, _json={u'follow_request_sent': False, u'has_extended_profile': False, u'profile_use_background_image': True, u'default_profile_image': False, u'id': 4493375957, u'profile_background_image_url_https': None, u'verified': False, u'profile_text_color': u'333333', u'profile_image_url_https': u'https://pbs.twimg.com/profile_images/676792892795584512/LO8qSzv2_normal.jpg', u'profile_sidebar_fill_color': u'DDEEF6', u'entities': {u'description': {u'urls': []}}, u'followers_count': 51, u'profile_sidebar_border_color': u'C0DEED', u'id_str': u'4493375957', u'profile_background_color': u'F5F8FA', u'listed_count': 43, u'is_translation_enabled': False, u'utc_offset': -25200, u'statuses_count': 10177, u'description': u'The leading source for trustworthy and timely health and medical news and information', u'friends_count': 88, u'location': u'', u'profile_link_color': u'2B7BB9', u'profile_image_url': u'http://pbs.twimg.com/profile_images/676792892795584512/LO8qSzv2_normal.jpg', u'following': False, u'geo_enabled': False, u'profile_banner_url': u'https://pbs.twimg.com/profile_banners/4493375957/1450194990', u'profile_background_image_url': None, u'screen_name': u'Nmedlife', u'lang': u'ro', u'profile_background_tile': False, u'favourites_count': 0, u'name': u'MedLife', u'notifications': False, u'url': None, u'created_at': u'Tue Dec 15 15:37:35 +0000 2015', u'contributors_enabled': False, u'time_zone': u'Pacific Time (US & Canada)', u'protected': False, u'default_profile': True, u'is_translator': False}, time_zone=u'Pacific Time (US & Canada)', id=4493375957, _api=<tweepy.api.API object at 0x7fcb41efb810>, verified=False, profile_text_color=u'333333', profile_image_url_https=u'https://pbs.twimg.com/profile_images/676792892795584512/LO8qSzv2_normal.jpg', profile_sidebar_fill_color=u'DDEEF6', is_translator=False, geo_enabled=False, entities={u'description': {u'urls': []}}, followers_count=51, protected=False, id_str=u'4493375957', default_profile_image=False, listed_count=43, lang=u'ro', utc_offset=-25200, statuses_count=10177, description=u'The leading source for trustworthy and timely health and medical news and information', friends_count=88, profile_link_color=u'2B7BB9', profile_image_url=u'http://pbs.twimg.com/profile_images/676792892795584512/LO8qSzv2_normal.jpg', notifications=False, profile_background_image_url_https=None, profile_background_color=u'F5F8FA', profile_banner_url=u'https://pbs.twimg.com/profile_banners/4493375957/1450194990', profile_background_image_url=None, name=u'MedLife', is_translation_enabled=False, profile_background_tile=False, favourites_count=0, screen_name=u'Nmedlife', url=None, created_at=datetime.datetime(2015, 12, 15, 15, 37, 35), contributors_enabled=False, location=u'', profile_sidebar_border_color=u'C0DEED', default_profile=True, following=False),
        #  _json={u'contributors': None, u'truncated': False, u'text': u"Medical News Today: 'Enhanced' environment boosts mouse immune systems: Breaking research demo... https://t.co/WyKB9E5Th0 #medical #news", u'is_quote_status': False, u'in_reply_to_status_id': None, u'id': 782157108338839552, u'favorite_count': 1, u'entities': {u'symbols': [], u'user_mentions': [], u'hashtags': [{u'indices': [122, 130], u'text': u'medical'}, {u'indices': [131, 136], u'text': u'news'}], u'urls': [{u'url': u'https://t.co/WyKB9E5Th0', u'indices': [98, 121], u'expanded_url': u'http://sh.st/1YJw6', u'display_url': u'sh.st/1YJw6'}]}, u'retweeted': False, u'coordinates': None, u'source': u'<a href="http://twitterfeed.com" rel="nofollow">twitterfeed</a>', u'in_reply_to_screen_name': None, u'in_reply_to_user_id': None, u'retweet_count': 0, u'id_str': u'782157108338839552', u'favorited': False, u'user': {u'follow_request_sent': False, u'has_extended_profile': False, u'profile_use_background_image': True, u'default_profile_image': False, u'id': 4493375957, u'profile_background_image_url_https': None, u'verified': False, u'profile_text_color': u'333333', u'profile_image_url_https': u'https://pbs.twimg.com/profile_images/676792892795584512/LO8qSzv2_normal.jpg', u'profile_sidebar_fill_color': u'DDEEF6', u'entities': {u'description': {u'urls': []}}, u'followers_count': 51, u'profile_sidebar_border_color': u'C0DEED', u'id_str': u'4493375957', u'profile_background_color': u'F5F8FA', u'listed_count': 43, u'is_translation_enabled': False, u'utc_offset': -25200, u'statuses_count': 10177, u'description': u'The leading source for trustworthy and timely health and medical news and information', u'friends_count': 88, u'location': u'', u'profile_link_color': u'2B7BB9', u'profile_image_url': u'http://pbs.twimg.com/profile_images/676792892795584512/LO8qSzv2_normal.jpg', u'following': False, u'geo_enabled': False, u'profile_banner_url': u'https://pbs.twimg.com/profile_banners/4493375957/1450194990', u'profile_background_image_url': None, u'screen_name': u'Nmedlife', u'lang': u'ro', u'profile_background_tile': False, u'favourites_count': 0, u'name': u'MedLife', u'notifications': False, u'url': None, u'created_at': u'Tue Dec 15 15:37:35 +0000 2015', u'contributors_enabled': False, u'time_zone': u'Pacific Time (US & Canada)', u'protected': False, u'default_profile': True, u'is_translator': False}, u'geo': None, u'in_reply_to_user_id_str': None, u'possibly_sensitive': False, u'lang': u'en', u'created_at': u'Sat Oct 01 09:56:01 +0000 2016', u'in_reply_to_status_id_str': None, u'place': None, u'metadata': {u'iso_language_code': u'en', u'result_type': u'recent'}}, coordinates=None, entities={u'symbols': [], u'user_mentions': [], u'hashtags': [{u'indices': [122, 130], u'text': u'medical'}, {u'indices': [131, 136], u'text': u'news'}], u'urls': [{u'url': u'https://t.co/WyKB9E5Th0', u'indices': [98, 121], u'expanded_url': u'http://sh.st/1YJw6', u'display_url': u'sh.st/1YJw6'}]}, in_reply_to_screen_name=None, id_str=u'782157108338839552', retweet_count=0, in_reply_to_user_id=None, favorited=False, source_url=u'http://twitterfeed.com', user=User(follow_request_sent=False, has_extended_profile=False, profile_use_background_image=True, _json={u'follow_request_sent': False, u'has_extended_profile': False, u'profile_use_background_image': True, u'default_profile_image': False, u'id': 4493375957, u'profile_background_image_url_https': None, u'verified': False, u'profile_text_color': u'333333', u'profile_image_url_https': u'https://pbs.twimg.com/profile_images/676792892795584512/LO8qSzv2_normal.jpg', u'profile_sidebar_fill_color': u'DDEEF6', u'entities': {u'description': {u'urls': []}}, u'followers_count': 51, u'profile_sidebar_border_color': u'C0DEED', u'id_str': u'4493375957', u'profile_background_color': u'F5F8FA', u'listed_count': 43, u'is_translation_enabled': False, u'utc_offset': -25200, u'statuses_count': 10177, u'description': u'The leading source for trustworthy and timely health and medical news and information', u'friends_count': 88, u'location': u'', u'profile_link_color': u'2B7BB9', u'profile_image_url': u'http://pbs.twimg.com/profile_images/676792892795584512/LO8qSzv2_normal.jpg', u'following': False, u'geo_enabled': False, u'profile_banner_url': u'https://pbs.twimg.com/profile_banners/4493375957/1450194990', u'profile_background_image_url': None, u'screen_name': u'Nmedlife', u'lang': u'ro', u'profile_background_tile': False, u'favourites_count': 0, u'name': u'MedLife', u'notifications': False, u'url': None, u'created_at': u'Tue Dec 15 15:37:35 +0000 2015', u'contributors_enabled': False, u'time_zone': u'Pacific Time (US & Canada)', u'protected': False, u'default_profile': True, u'is_translator': False}, time_zone=u'Pacific Time (US & Canada)', id=4493375957, _api=<tweepy.api.API object at 0x7fcb41efb810>, verified=False, profile_text_color=u'333333', profile_image_url_https=u'https://pbs.twimg.com/profile_images/676792892795584512/LO8qSzv2_normal.jpg', profile_sidebar_fill_color=u'DDEEF6', is_translator=False, geo_enabled=False, entities={u'description': {u'urls': []}}, followers_count=51, protected=False, id_str=u'4493375957', default_profile_image=False, listed_count=43, lang=u'ro', utc_offset=-25200, statuses_count=10177, description=u'The leading source for trustworthy and timely health and medical news and information', friends_count=88, profile_link_color=u'2B7BB9', profile_image_url=u'http://pbs.twimg.com/profile_images/676792892795584512/LO8qSzv2_normal.jpg', notifications=False, profile_background_image_url_https=None, profile_background_color=u'F5F8FA', profile_banner_url=u'https://pbs.twimg.com/profile_banners/4493375957/1450194990', profile_background_image_url=None, name=u'MedLife', is_translation_enabled=False, profile_background_tile=False, favourites_count=0, screen_name=u'Nmedlife', url=None, created_at=datetime.datetime(2015, 12, 15, 15, 37, 35), contributors_enabled=False, location=u'', profile_sidebar_border_color=u'C0DEED', default_profile=True, following=False), geo=None, in_reply_to_user_id_str=None, possibly_sensitive=False, lang=u'en', created_at=datetime.datetime(2016, 10, 1, 9, 56, 1), in_reply_to_status_id_str=None, place=None, source=u'twitterfeed', retweeted=False, metadata={u'iso_language_code': u'en', u'result_type': u'recent'})
