class Author:
    def __init__(self):
        self.id = None
        self.screen_name = None
        self.verified = None
        self.statuses_count = None
        self.friends_count = None
        self.lang = None
        self.created_at = None
    def extract(self, tweepy_author):
        self.id = tweepy_author.id
        self.screen_name = tweepy_author.screen_name.encode('UTF-8')
        self.verified = tweepy_author.verified
        self.statuses_count = tweepy_author.statuses_count
        self.friends_count = tweepy_author.friends_count
        self.lang = tweepy_author.lang.encode('UTF-8')
        self.created_at = tweepy_author.created_at