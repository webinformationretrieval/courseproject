import time

import tweepy
import db
from conf import parameters, uparameters
from model.tweet import Tweet
import model.rate_status as rstatus
import iocsv

api = None
used_api = 0

def authenticate():
    global api
    auth = tweepy.OAuthHandler(parameters.twitter_key, parameters.twitter_secret)

    if (uparameters.twitter_token is None or uparameters.twitter_token_secret is None):
        print ("Access token or Access token secret are not supplied")
        exit(1)

    auth.set_access_token(uparameters.twitter_token, uparameters.twitter_token_secret)

    api = tweepy.API(auth)

def search_latest():
    global api
    tweepy_tweets = api.home_timeline(count=200)
    return normalizeTweets(tweepy_tweets)

def search_by_hashtag(hashtag):
    global api
    tweepy_tweets = api.search("#" + hashtag, count=200)
    return normalizeTweets(tweepy_tweets)

def search_by_term(term):
    global api
    tweepy_tweets = api.search(term, count=200, lang="en")
    return normalizeTweets(tweepy_tweets)

def search_by_screenname(screenname, page=1):
    global api
    tweepy_tweets = api.user_timeline(screen_name=screenname, count=200, page=page) # 200 is the maximum
    return normalizeTweets(tweepy_tweets)

def crawl_by_screenname(screenname, max_pages=1, health=None, rumor=None, save_to=None):
    global api
    page = 1
    start_time = time.time()

    rate_status = rstatus.get_rate_status(api)

    remaining = rate_status.remaining

    while page <= max_pages:
        if remaining == 0:
            sleep_time = rate_status.reset - time.time()
            if sleep_time > 0:
                print ("Sleeping until reset time: " + str(sleep_time) + " seconds")
                time.sleep(sleep_time)
                print ("Resumed")
        print ("Requested tweets for %s [%d\%d]" % (screenname, page, max_pages))
        tweets = search_by_screenname(screenname, page=page)
        rate_status.decrement_remaining()
        if save_to is not None:
            db.add_tweets(tweets, db_type=save_to, is_health=health, is_rumor=rumor)
        page += 1
        time.sleep(parameters.api_limit_duration / parameters.api_limit)

def get_by_tweets_id(csv_file):
    global api

    rate_status = rstatus.get_rate_status(api, True)
    remaining = rate_status.remaining

    rows = iocsv.read_file(csv_file, '\t')
    tweet_group = []
    for i in range (0, len(rows) / 100 + 1):
        tweet_group.append([])

    for i, row in enumerate(rows):
        group = i / 100
        tweet_group[group].append(row[0])

    for row in tweet_group:
        if remaining == 0:
            sleep_time = rate_status.reset - time.time()
            if sleep_time > 0:
                print ("Sleeping until reset time: " + str(sleep_time) + " seconds")
                time.sleep(sleep_time)
                print ("Resumed")
        print ("Requesting 100 tweets into %s"%iocsv.TWEETS)
        tweets = api.statuses_lookup(row)
        tweets = normalizeTweets(tweets)
        rate_status.decrement_remaining()
        iocsv.write_tweets(iocsv.TWEETS, tweets)
        time.sleep(parameters.api_limit_duration / parameters.api_limit)

def normalizeTweets(tweepy_tweets):
    tweets = []
    for tt in tweepy_tweets:
        t = Tweet()
        t.extract_from_tweepy(tt)
        tweets.append(t)

    return tweets

def print_tweets(tweets):
    if tweets is None:
        return

    for t in tweets:
        print t.text

### END DEF

authenticate()