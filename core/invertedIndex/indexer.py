import csv

class Searcher:

    def search(self, term):
        return self.map[term]

    def create_index_dict(self, texts):
        self.map = {}
        for index, text in enumerate(texts):
            print text
            for word in text:
                if word in self.map.keys():
                    self.map.get(word).append(index)
                else:
                    self.map[word] = [index]

        freq = [(key, len(value)) for (key, value) in self.map.items()]
        pop = [key for (key, value) in sorted(freq, key=lambda x: x[1], reverse=True)]
        pop = [x for x in pop if len(x) > 1][:5]
        print pop
        print self.map

        self.write_file("indexed.csv", self.map)
        return map, pop


    def write_file(self, file_name, dict):
        with open(file_name, "w") as csvfile:
            writer = csv.writer(csvfile, delimiter=":")
            for key, value in dict.items():
                writer.writerow([key, '->'.join(str(x) for x in value)])
