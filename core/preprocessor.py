from nltk.corpus import stopwords
import urlparse
import re



def remove_additional(string):
    words = string.split()
    stop = set(stopwords.words('english'))
    new_words = []
    for word in words:
        if is_url(word):
            continue
        elif is_retweet(word):
            continue
        elif word in stop:
            continue
        elif word.isdigit():
            continue
        elif has_hashtag(word):
            continue
        elif has_hyphen(word):
            continue
        elif has_claim(word):
            continue
        elif has_at(word):
            continue
        elif has_at(word):
            continue
        elif len(word) == 1:
            continue
        elif any(ch.isdigit() for ch in word):#remove words that contains digits.
            continue
        word = re.sub(r'(.)\1+', r'\1\1',word) #remove words with repeated letters.
        new_words.append(word.lower())

    return ' '.join(new_words)

def is_url(string):
    url = string
    try:
        parts = urlparse.urlparse(url)
        if not parts.scheme or not parts.netloc:
            return False
        else:
            return True
    except:
        return False

def has_hashtag(string):
    return "#" in string

def has_at(string):
    return "@" in string

def has_hyphen(string):
    return "-" in string

def has_claim(string):
    return ":" in string

def get_words(string):
    return re.sub("[^\w]", " ", string).split()

def is_retweet(string):
    return "RT" in string


def preprocess(string):
    string = remove_emoji(string)
    string = remove_additional(string)
    words = get_words(string)

    return words

def remove_emoji(data):
    "remove emoji in the string"
    if not data:
        return data
    if not isinstance(data, basestring):
        return data
    try:
    # UCS-4
        patt = re.compile(u'([\U00002600-\U000027BF])|([\U0001f300-\U0001f64F])|([\U0001f680-\U0001f6FF])')
    except re.error:
    # UCS-2
        patt = re.compile(u'([\u2600-\u27BF])|([\uD83C][\uDF00-\uDFFF])|([\uD83D][\uDC00-\uDE4F])|([\uD83D][\uDE80-\uDEFF])')
    return patt.sub('', data)