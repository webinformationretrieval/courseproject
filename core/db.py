import sqlite3 as lite

TRAINING="training"
TESTING="testing"

tweet="tweet"
author="author"
testing_tweet="test_tweet"
testing_author="test_author"

con = None

def connect():
    """
    Connects to the database project.db
    """
    global con
    try:
        con = lite.connect('project.db')
    except lite.Error, e:
        print ("Error connecting to database: %s" % e.args[0])

    return con

def get_connection():
    """
    Gets an existing connection or creates a new database connection
    """
    global con
    if con is None:
        connect()
    return con

def add_tweets(tweets, db_type=None, is_health=None, is_rumor=None):
    if db_type is None:
        raise Exception('Database type is not specified')

    con = get_connection()
    c = con.cursor()
    for tweet in tweets:
        try:
            c.execute('INSERT INTO %s (tweet_id, author_id, text, rawtext, favorite_count, retweet_count, lang, created_at, health, rumor) VALUES (?,?,?,?,?,?,?,?,?,?)'%get_tweet_table_name(db_type), (tweet.id, tweet.author.id, None, tweet.text, \
                  tweet.favorite_count, tweet.retweet_count, tweet.lang, tweet.created_at, is_health, is_rumor))
        except lite.Error, e:
            print ("Warning: Error inserting tweet %d: %s" % (tweet.id, e.args[0]))

        add_authors([tweet.author], db_type=db_type, is_health=is_health, is_rumor=is_rumor)
    con.commit()

def add_authors(authors, db_type=None, is_health=None, is_rumor=None):
    if db_type is None:
        raise Exception('Database type is not specified')

    con = get_connection()
    c = con.cursor()
    for author in authors:
        try:
            c.execute('INSERT INTO %s VALUES (?,?,?,?,?,?,?,?,?)'%get_author_table_name(db_type), [author.id, author.screen_name, author.verified, \
                  author.statuses_count, author.friends_count, author.lang, author.created_at, is_health, is_rumor])
        except lite.Error, e:
            print ("Warning: Error inserting author %d: %s" % (author.id, e.args[0]))
    con.commit()

def get_tweet_table_name(db_type):
    if db_type is TRAINING:
        return tweet
    elif db_type is TESTING:
        return testing_tweet

def get_author_table_name(db_type):
    if db_type is TRAINING:
        return author
    elif db_type is TESTING:
        return testing_author

def get_health_type_tweets(is_health, db_type=TRAINING, limit=10000):
    con = get_connection()
    c = con.cursor()
    try:
        c.execute('SELECT * FROM %s WHERE health=?'%get_tweet_table_name(db_type), [is_health])
    except lite.Error, e:
        print ("Warning: Cannot select health type tweets: %s" % (e.args[0]))
    return c.fetchall()

def get_rumor_type_tweets(is_rumor, db_type=TRAINING, limit=10000):
    con = get_connection()
    c = con.cursor()
    try:
        c.execute('SELECT * FROM %s WHERE rumor=?'%get_tweet_table_name(db_type), [is_rumor])
    except lite.Error, e:
        print ("Warning: Cannot select rumor type tweets: %s" % (e.args[0]))
    return c.fetchall()