import cv2
import numpy
import numpy.random

img = cv2.imread("Lenna.png").astype(numpy.float32)
gaussianNoise = numpy.random.normal(loc=0,scale=1.0,size=img.shape)
imgNoised = img+gaussianNoise

