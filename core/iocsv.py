import csv
import db
from model.tweet import Tweet

TRAINING = "training.csv"
TESTING= "testing.csv"
TWEETS = "tweets.csv"

def print_file(file_name):
    with open(file_name, 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        for row in reader:
            print row

def write_line(file_name, data):
    with open(file_name, 'a') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        writer.writerow(data)

def write_tweets(file_name, tweets):
    with open(file_name, 'a') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for tweet in tweets:
            writer.writerow(tweet.items())

def write_file(file_name, tweets):
    with open(file_name, "w") as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        for tweet in tweets:
            writer.writerow(tweet.items())

def read_file(file_name, delimiter=','):
    with open(file_name, 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=delimiter)
        rows = []
        for row in reader:
            rows.append(row)

    return rows

def read_tweets_by_index(file_name, indexes, delimiter=','):
    with open(file_name, 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=delimiter)
        tweets = []
        for index, row in enumerate(reader):
            if index in indexes:
                tweet = Tweet()
                tweet.extract_from_csv(row)
                tweets.append(tweet)

    return tweets

def read_tweets(file_name, delimiter=','):
    with open(file_name, 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=delimiter)
        tweets = []
        for row in reader:
            tweet = Tweet()
            tweet.extract_from_csv(row)
            tweets.append(tweet)

    return tweets

def move_tweets_to_db(file_name, is_health=None, is_rumor=None):
    raise NotImplementedError('Implement your own for your own case')

    print "Moving tweets from csv %s to DB"%file_name
    with open(file_name, 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        for row in reader:
            # Enter your parsing here
            db.add_tweets([tweet], is_health, is_rumor)

    print "Finished moving tweets"