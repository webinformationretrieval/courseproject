import classifierNB
import csv
import definitions
import dataImporter
clf = classifierNB.TopicClassifier()

# testing on the zikavirus dataset
def get_health_tweets():
    path = definitions.ROOT_DIR + "/rawDatasets/dataset_2016/tweets.csv"
    tweetsFD = open(path, "rb")
    tweets_r = [row for row in csv.reader(tweetsFD, delimiter='\t', quotechar='|')][2000:]
    tweetsFD.close()
    tweets = [x[3] for x in tweets_r]
    return tweets

d = dataImporter.load('comp.graphics')
print(len(d))

fd = [x for x in d if clf.is_health_related(" ".join(x))]
print(len(fd))

dt = dataImporter.load('talk.politics.mideast')
print(len(dt))

fdd = [x for x in dt if clf.is_health_related(" ".join(x))]
print(len(fdd))

tweets = get_health_tweets()
print(len(tweets))
ex = tweets[1]
print(ex)
print(clf.is_health_related(ex))
clf.probabilities(ex)

filtered = [x for x in tweets if clf.is_health_related(x)]
print(len(filtered))