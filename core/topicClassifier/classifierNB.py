import pickle
import os
import core.preprocessor as preprocessor

class TopicClassifier:

    def __init__(self):
        path = os.path.dirname(os.path.realpath(__file__))
        MultinomialNB = path + "/MultinomialNB.pickle"
        classifier_f = open(MultinomialNB, "rb")
        classifier = pickle.load(classifier_f)
        classifier_f.close()

        self.classifier = classifier

        with open(path + '/word_features', 'rb') as f:
            self.word_features = pickle.load(f)


    def find_features(self, document):
        words = set(document)
        features = {}
        for w in self.word_features:
            features[w] = int(w in words)

        return features

    def is_health_related(self,text):
        features = self.find_features(preprocessor.preprocess(text))
        if self.classifier.classify(features) == 'sci.med':
            # defining a min prob
            return self.classifier.prob_classify(features)._prob_dict['sci.med'] > 0.4

        return False

    def probabilities(self, text):
        features = self.find_features(preprocessor.preprocess(text))
        dict = self.classifier.prob_classify(features)._prob_dict
        for key, value in dict.iteritems():
            print (key, value*100)
        return dict