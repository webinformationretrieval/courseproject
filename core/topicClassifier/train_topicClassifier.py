import random
import nltk
import math
import pickle
import dataImporter
import sklearn.decomposition
from nltk.classify.scikitlearn import SklearnClassifier
from sklearn.naive_bayes import MultinomialNB,BernoulliNB
from sklearn.linear_model import LogisticRegression,SGDClassifier
from sklearn.svm import SVC, LinearSVC, NuSVC

categories = [
        "alt.atheism",
        # "comp.graphics",
        "comp.os.ms-windows.misc",
        # "comp.sys.ibm.pc.hardware",
        # "comp.sys.mac.hardware",
        # "comp.windows.x",
        "misc.forsale",
        # "rec.autos",
        # "rec.motorcycles",
        "rec.sport.baseball",
        # "rec.sport.hockey",
        # "sci.crypt",
        # "sci.electronics",
        "sci.med",
        # "sci.space",
        "soc.religion.christian",
        # "talk.politics.guns",
        # "talk.politics.mideast",
        "talk.politics.misc"
        # "talk.religion.misc"
]

documents =[]
all_words = []
health_words = []
for category in categories:
    print(category)
    texts = dataImporter.load(category)
    for text in texts:
        all_words.extend(text)
        documents.append((text, category))

random.shuffle(documents)
all_words = nltk.FreqDist(all_words)
word_features = list(all_words.keys())[:6000]

# add health-related words to features
def read_health_words():
    return [word.lower() for line in open('health_voc.txt', 'r') for word in line.split()]

# word_features.extend(read_health_words())

# write features to file
with open('word_features', 'wb') as f:
    pickle.dump(word_features, f)

# feature dictionary from email
def find_features(document):
    words = set(document)
    features = {}
    for w in word_features:
        features[w] = int(w in words)

    return features

# feature vector from email
def find_feature_vector(document):
    words = set(document)
    features = []
    for w in word_features:
        # boolean vectors
        features.append(int(w in words))

    return features

# PCA - not used at the moment
# model = sklearn.decomposition.PCA(n_components=400)
# feature_vectors = [find_feature_vector(email) for (email, _) in documents]
# fittedModel = model.fit(feature_vectors)
# transformed_features = fittedModel.transform(feature_vectors)
# category_list = [category for (_, category) in documents]

# save fitted model
# save_fittedModel = open("PCA_model.pickle", "wb")
# pickle.dump(fittedModel, save_fittedModel)
# save_fittedModel.close()

# featuresets = zip(transformed_features.tolist(), category_list)
featuresets = [(find_features(email), category) for (email, category) in documents]

# dividing the featuresets to training and testing 7:3
index = int(math.floor(len(featuresets)*0.7))
training_set = featuresets[:index]
testing_set = featuresets[index:]

# defining classifiers
classifiers = {
    'LogisticRegression': SklearnClassifier(LogisticRegression()),
    'MultinomialNB': SklearnClassifier(MultinomialNB())
}

# training classifiers
for classifier in classifiers.itervalues():
    classifier.train(training_set)

classifiers['NaiveBayes_nltk'] = nltk.NaiveBayesClassifier.train(training_set)

# analysis
for name, classifier in classifiers.iteritems():
    print(name + " accuracy percent:", (nltk.classify.accuracy(classifier, testing_set)) * 100)

# features
def most_informative_feature_for_med(classifier, n=10):
    labelid = classifier.labels().index('sci.med')
    topn = sorted(zip(classifier._clf.coef_[labelid], word_features))[-n:]
    for coef, feat in topn:
        print 'sci.med', feat, coef


for name, classifier in classifiers.iteritems():
    print(name)
    try:
        most_informative_feature_for_med(classifier)
    except:
        print 'N/A'
    try:
        classifier.show_most_informative_features(20)
    except:
        print 'N/A'

# save classifiers
for name, classifier in classifiers.iteritems():
    save_classifier = open(name + ".pickle","wb")
    pickle.dump(classifier, save_classifier)
    save_classifier.close()
