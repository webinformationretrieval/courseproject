import core.preprocessor as preprocessor
import glob
import definitions
import csv

def load(category):
    path = definitions.ROOT_DIR + '/20_newsgroups/'+category+'/*'
    files = glob.glob(path)
    contents = []
    for file in files:
        f = open(file, 'r')
        contents.append(f.read())
        f.close()

    refined = refine_data(contents)
    if category == 'sci.med':
        tweets = get_health_tweets()
        cleaned = [preprocessor.preprocess(tweet) for tweet in tweets]
        refined.extend(cleaned)

    return refined

#zikavirus dataset
def get_health_tweets():
    path = definitions.ROOT_DIR + "/rawDatasets/dataset_2016/tweets.csv"
    tweetsFD = open(path, "rb")
    tweets_r = [row for row in csv.reader(tweetsFD, delimiter='\t', quotechar='|')][:2000]
    tweetsFD.close()
    tweets = [x[3] for x in tweets_r]
    return tweets

def refine_data(file_data):
    for i, text in zip(range(len(file_data)) ,file_data):
        file_data[i] = preprocessor.preprocess(clean_instance(text))

    return file_data

def clean_instance(text):
    parts = text.split('\n')
    newparts = []

    finished = False
    for part in parts:
        if finished:
            newparts.append(part)
            continue
        if part.startswith('Lines:'):
            finished = True

    return '\n'.join(newparts)