import crawler
import argparse
import iocsv, db

def main():
    parser = argparse.ArgumentParser(description='Crawl medical tweets')

    search_group = parser.add_mutually_exclusive_group()
    search_group.add_argument("--screenname", dest="screenname", help="Crawls by screenname")
    search_group.add_argument("--hashtag", dest="hashtag", help="Search by hashtag")
    search_group.add_argument("--term", dest="term", help="Search by terms")
    search_group.add_argument("--tweets", dest="tweets", help="Get tweets by tweets id from a csv file")
    search_group.add_argument("--move-to-db", dest="move_to_db", help="Moves the tweets from CSV to DB")

    healthy_group = parser.add_mutually_exclusive_group()
    healthy_group.add_argument("--health", action="store_true", dest="health", help="Label data as health-related")
    healthy_group.add_argument("--not-health", action="store_true", dest="not_health", help="Label data as not health-related")

    rumor_group = parser.add_mutually_exclusive_group()
    rumor_group.add_argument("--rumor", action="store_true", dest="rumor", help="Label data as rumor")
    rumor_group.add_argument("--not-rumor", action="store_true", dest="not_rumor", help="Label data as not rumor")

    parser.add_argument("--max", dest="max_pages", help="Max number of pages to retrieve", default=1)

    save_group = parser.add_mutually_exclusive_group()
    save_group.add_argument("-s", "--save-training", action="store_true", dest="save_to_training",
                            help="Saves the tweets in the training database")
    save_group.add_argument("-t", "--save-testing",
                            action="store_true", dest="save_to_testing",
                            help="Saves the tweets in the testing database")

    args = parser.parse_args()

    health = None
    if args.health is True:
        health = True
    elif args.not_health is True:
        health = False

    rumor = None
    if args.rumor is True:
        rumor = True
    elif args.not_rumor is True:
        rumor = False

    save_to = None
    if args.save_to_training is True:
        save_to = db.TRAINING
    elif args.save_to_testing is True:
        save_to = db.TESTING

    tweets = None
    if args.screenname is not None:
        tweets = crawler.crawl_by_screenname(args.screenname, max_pages=int(args.max_pages), health=health, rumor=rumor, save_to=save_to)
    elif args.hashtag is not None:
        tweets = crawler.search_by_hashtag(args.hashtag)
    elif args.tweets is not None:
        tweets = crawler.get_by_tweets_id(args.tweets)
    elif args.move_to_db is not None:
        iocsv.move_tweets_to_db(args.move_to_db, is_health=health, is_rumor=rumor)
    else:
        parser.print_help()

<<<<<<< HEAD
    if args.save_training is True:
        iocsv.write_tweets('training_.csv', tweets)

=======
>>>>>>> a93307838a1aafc03c106c1dad018c93a0c0817e
    crawler.print_tweets(tweets)

if __name__ == "__main__":
    main()