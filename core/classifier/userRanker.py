from core.classifier.models import CharacterOrientedTextBasedClassifier
import csv

def estimateUserRank(probabilitiesUser):
    # User Rank estimated as an estimate of the a-priori probability of posting a rumor
    meanPrediction = 0
    counts = 0
    for i in probabilitiesUser:
        meanPrediction+=i
        counts+=1
    return meanPrediction/counts
        
if __name__=="__main__":
    ###### Dataset importing step ######
    fd = open("rawDatasets/NodeXL/users-last-10-tweetsALL_predicted.csv","rb")
    csvReader = csv.reader(fd,delimiter=';')
    data = []
    header = csvReader.next()
    header.append("UserRank")
    for i in csvReader:
        data.append(i)
    usersAndRumorProbability = [[i[0],i[-1]] for i in data]
    userDictionary = {}
    for i in usersAndRumorProbability:
        if i[0] not in userDictionary:
            userDictionary[i[0]]=[]
        if(i[1]!=""):
            userDictionary[i[0]].append(float(i[1]))
    ####################################
    userEstimatedRanks = {}
    for user in userDictionary:
        userEstimatedRanks[user]=estimateUserRank(userDictionary[user])
    # Adding the user ranks to the graph
    for i in data:
        i.append(userEstimatedRanks[i[0]])
    writer = open("rawDatasets/NodeXL/users-last-10-tweetsALL_predicted_withUserRank.csv","wb")
    csvWriter = csv.writer(writer,delimiter=';')
    csvWriter.writerow(header)
    for i in data:
        csvWriter.writerow(i)
    writer.flush()
    writer.close()
    