import theano
import lasagne
import csv
import os
import numpy
import random
import tqdm
import pickle
import re
from gensim.models import Word2Vec
import sklearn
import sklearn.cross_validation
from sklearn.cross_validation import KFold

# Theano float policy
theano.config.floatX = 'float32'

# Reproducibility
random.seed(1)
numpy.random.seed(1)

# Hyper-parameters
n_terms = 241
epochs = 30
batchSize = 10
learningRate = 0.01
testSetExamples = 112
savingFrequency = 2 # Save every <savingFrequency> epochs 
modelName = "textBasedClassifier_characterOriented"
dictionary = "abcdefghijklmnopqrstuvwxyz@# "
dictionaryToIndex = {t:i+2 for i,t in enumerate(dictionary)}

def Qazvinian_etal2011_dataset_handler():
    # Not used in this implementation
    rawDatasetLocation = "rawDatasets/Qazvinian_etal2011_dataset/"
    
    # Retrieving airfrance.csv
    # header = keyword	|UserName|UserID|TweetText|TweetID|DateOfPublication|#ofFollowers|#ofFollowings	|#ofStatuses|URLs|Label
    airfranceFD = open(os.path.join(rawDatasetLocation,"airfrance.txt"))
    airfrance = [row for row in csv.reader(airfranceFD,delimiter='\t',quotechar='|')]
    airfranceFD.close()
    
    michelleFD = open(os.path.join(rawDatasetLocation,"michelle.txt"))
    michelle = [row for row in csv.reader(michelleFD,delimiter='\t',quotechar='|')]
    michelleFD.close()
    
    palinFD = open(os.path.join(rawDatasetLocation,"palin.txt"))
    palin = [row for row in csv.reader(palinFD,delimiter='\t',quotechar='|')]
    palinFD.close()
    
    # header = TweetID|Label
    cellIdsFD = open(os.path.join(rawDatasetLocation,"cell-ids.txt"))
    cellIds = [row for row in csv.reader(cellIdsFD,delimiter='\t',quotechar='|')]
    cellIds.close()
    
    obamaIdsFD = open(os.path.join(rawDatasetLocation,"obama-ids.txt"))
    obamaIds = [row for row in csv.reader(obamaIdsFD,delimiter='\t',quotechar='|')]
    obamaIdsFD.close()
    
    return airfrance, michelle, palin, cellIds, obamaIds
    

def dataset_2016_handler():
    rawDatasetLocation = "rawDatasets/dataset_2016/"
    
    # Retrieving tweets.csv
    # header = keyword	|UserName|UserID|TweetText|TweetID|DateOfPublication|#ofFollowers|#ofFollowings	|#ofStatuses|URLs|Label
    
    tweetsFD = open(os.path.join(rawDatasetLocation,"tweets.csv"))
    tweets = [row for row in csv.reader(tweetsFD,delimiter='\t',quotechar='|')]
    tweetsFD.close()
    
    # Retrieving retweets.csv
    # header = keyword	|UserName|UserID|TweetText|TweetID|DateOfPublication|OriginalAuthorUserName|OriginalAuthorUserID|OriginalTweetText|TweetIdOriginalMicroblog
    retweetsFD = open(os.path.join(rawDatasetLocation,"retweets.csv"))
    retweets = [row for row in csv.reader(retweetsFD,delimiter='\t',quotechar='|')]
    retweetsFD.close()
    
    # Retrieving replies.csv
    # header = keyword|ReplyUserName|ReplyUserID|ReplyTweetID|ReplyText|ReplyDate|RepliedUserName|RepliedUserID|RepliedTweetID|RepliedText
    repliesFD = open(os.path.join(rawDatasetLocation,"replies.csv"))
    replies = [row for row in csv.reader(repliesFD,delimiter='\t',quotechar='|')]
    repliesFD.close()
    
    # Retrieving repliesLabel.csv
    # header = RepliedTweetID|ReplyTweetID|RepliedTweetLabel|ReplyTweetLabel
    repliesLabelsFD = open(os.path.join(rawDatasetLocation,"repliesLabel.csv"))
    repliesLabels = [row for row in csv.reader(repliesLabelsFD,delimiter='\t',quotechar='|')]
    repliesLabelsFD.close() 
    
    return tweets, retweets, replies, repliesLabels

def stemmer(tweetDataset):
    # Not used in this implementation
    dictionary = {}
    for tweet in tweetDataset:
        tweetWords = tweet.split(" ")
        for word in tweetWords:
            if(word not in dictionary):
                dictionary[word] = 0
            dictionary[word]+=1
    dictionary = dictionary.items()
    dictionary = sorted(dictionary,key=lambda word: word[1],reverse=True)
    print zip(*dictionary)
    
def buildModel(input_var):
    # converges in epoch 5, 72.5%
    net = lasagne.layers.InputLayer(input_var=input_var, shape=(None, None, n_terms))
    net = lasagne.layers.LSTMLayer(
            net, 32, grad_clipping=50,
            nonlinearity=lasagne.nonlinearities.tanh,
            only_return_final=True)
    net = lasagne.layers.DenseLayer(net, 
    num_units=2, W = lasagne.init.HeNormal(), 
    nonlinearity = lasagne.nonlinearities.softmax)
    return net


def getData(p, X, y, batchSize = batchSize, return_target=True):
    batchSize = min(batchSize,len(X)-p)
    maxSequenceSize = 0
    for i in X[p:p+batchSize]:
        if(len(i)>maxSequenceSize):
            maxSequenceSize = len(i)
    x_result = numpy.zeros((batchSize,maxSequenceSize,n_terms), dtype=numpy.float32)
    y_result = numpy.zeros(batchSize, dtype=numpy.int32)
    for i in xrange(batchSize):
        for j in xrange(maxSequenceSize):
            x_result[i,j] = [1 if j==0 else 0 for j in xrange(n_terms)]
    for n in range(batchSize):
        for m,i in enumerate(X[n+p]):
            x_result[n,m]=[1 if j==i else 0 for j in xrange(n_terms)]
        if(return_target):
            y_result[n] = y[p+n]
    return x_result, y_result

def getRandomSlice(v,size):
    start = random.randint(0,len(v)-size)
    end = start+size
    return v[start:end]

def getData_(p, X, y, batchSize = batchSize, return_target=True):
    minSequenceSize = 0
    for i in X[p:p+batchSize]:
        if(len(i)<minSequenceSize):
            minSequenceSize = len(i)
    x_result = numpy.zeros((batchSize,minSequenceSize,n_terms), dtype=numpy.float32)
    y_result = numpy.zeros(batchSize, dtype=numpy.int32)
    #for i in xrange(batchSize):
    #    for j in xrange(minSequenceSize):
    #        x_result[i,j] = [1 if j==0 else 0 for j in xrange(n_terms)]
    for n in range(batchSize):
        for m,i in enumerate(getRandomSlice(X[n+p],minSequenceSize)):
            x_result[n,m]=[1 if j==i else 0 for j in xrange(n_terms)]
        if(return_target):
            y_result[n] = y[p+n]
    return x_result, y_result
    
    
def ceildiv(a, b):
    return -(-a // b)

def inverse(s):
    for i in s:
        i.argmax(axis=1)

#def preprocessTweet(t):
#    

if __name__ == "__main__":
    tweets, retweets, replies, repliesLabels = dataset_2016_handler()

    # There are 572 rumors and 406 non-rumors
    # Unlabelled tweets = 3223

    X = [t[3] for t in tweets]
    y = [t[10] for t in tweets]

    # PREPROCESSING w.r.t. y
    # __________________________

    # Keeping only labelled tweets
    X_filtered = []
    y_filtered = []
    countR = 0
    countNR = 0
    for i in xrange(len(y)):
        if(y[i]=='R'):
            countR += 1
            X_filtered.append(X[i])
            y_filtered.append(1)
        elif(y[i]=='NR'):
            countNR += 1
            X_filtered.append(X[i])
            y_filtered.append(0)
    X = X_filtered
    y = y_filtered
    
    # Downsampling filter
    downsamplingFactor = min(countR,countNR)
    X_filtered = []
    y_filtered = []
    countR = 0
    countNR = 0
    for i in xrange(len(y)):
        if(y[i]==1 and countR < downsamplingFactor):
            countR += 1
            X_filtered.append(X[i])
            y_filtered.append(y[i])
        elif(y[i]==0 and countNR < downsamplingFactor):
            countNR += 1
            X_filtered.append(X[i])
            y_filtered.append(y[i])
    X = X_filtered
    y = y_filtered
    
    
    # Removing urls
    
    #urlRegex = 'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+'
    
    #for tweet in X:
    #    for i in re.findall(urlRegex,tweet):
    #        tweet=tweet.replace(i,"") 
    
    # PREPROCESSING w.r.t. X
    # __________________________

    # Converting letters into integers, we consider only unicode character below 240 (n_terms), the integer 1 is reserved for unknown characters and the 0 for termination.
    X_filtered = []
    for tweet in X:
        tmp = []
        for letter in tweet:
            letter = letter.lower()
            if(letter in set([i for i in dictionary])):
                tmp.append(letter)
        tmp = "".join(tmp)
        X_filtered.append(tmp)
    X = [[ord(letter)+2 if ord(letter)+2<=n_terms else 1 for letter in tweet] for tweet in X]
    X = X_filtered
    y = y_filtered
    
    #X_words = [tweet.split(" ") for tweet in X]
    
    #embedd = Word2Vec(iter=1)
    #embedd.build_vocab(X_words)
    #embedd.train(X_words)
    #X_=[]
    #for i,tweet in enumerate(X_words):
    #    print i,tweet
    #    X_.append([embedd[word] for word in tweet])
    #X = [[embedd[word] for word in tweet] for tweet in X_words]
    #n_terms = vocab.size
    n_terms = len(dictionaryToIndex)+2
    X = [[dictionaryToIndex[letter] if letter in dictionaryToIndex else 1 for letter in tweet] for tweet in X]
    #X = [i[:100] for i in X]
    # Shuffling data and balancing batches
    tmp = zip(X,y)
    random.shuffle(tmp)
    tmpRumor = []
    tmpNonRumor = []
    for example in tmp:
        if(example[1]==1):
            tmpRumor.append(example)
        else:
            tmpNonRumor.append(example)
    result = []
    for i in xrange(len(tmp)):
        if(i%2):
            result.append(tmpRumor[i/2])
        else:
            result.append(tmpNonRumor[i/2])
    X, y = zip(*result)
    # Building LSTM NN
    input_var = theano.tensor.tensor3(name="input", dtype=theano.config.floatX)
    target_var = theano.tensor.ivector('target')
    model = buildModel(input_var)
    
    # Compilation phase...
    prediction = lasagne.layers.get_output(model)
    loss = lasagne.objectives.categorical_crossentropy(prediction, target_var).mean()
    params_trainable = lasagne.layers.get_all_params(model,trainable=True)
    #all_grads = theano.tensor.grad(loss, params_trainable)
    #all_grads = [theano.tensor.clip(g, -1, 1) for g in all_grads]
    #all_grads = lasagne.updates.total_norm_constraint(
    #    all_grads, 0.1, return_norm=False)
    updates = lasagne.updates.adagrad(
            loss, params_trainable, learning_rate=learningRate)
    
    #parameters = numpy.load("textBasedClassifier_characterOriented_.npz")
    #param_values = [parameters['arr_%d' % i] for i in range(len(parameters.files))]
    #lasagne.layers.set_all_param_values(model, param_values)
    
    print "Compiling training function..."
    train_fn = theano.function([input_var, target_var], loss, updates=updates)
    
    deterministic_prediction = lasagne.layers.get_output(model, deterministic=True)
    deterministic_loss = lasagne.objectives.categorical_crossentropy(deterministic_prediction, target_var).mean()
    deterministic_acc = theano.tensor.eq(theano.tensor.argmax(deterministic_prediction, axis=1), target_var).mean()
    deterministic_out = theano.tensor.argmax(deterministic_prediction, axis=1)
    print "Compiling test function..."
    test_fn = theano.function([input_var, target_var], [deterministic_loss, deterministic_acc, deterministic_out,deterministic_prediction])
    
    # Training cycle
    kf = KFold(n=len(X),n_folds=5)
    X = numpy.array(X)
    y = numpy.array(y,dtype=int)
    kf_index=0
    for train_index, test_index in kf:
        X_train = X[train_index]
        y_train = y[train_index]
        X_test = X[test_index]
        y_test = y[test_index]
        print "_______________________________"
        for j in xrange(epochs):
            print "Epoch {}".format(j)
            print "Training phase..."
            trainIterations = 0
            avg_training_cost = 0.0
            for i in tqdm.tqdm(xrange(0,len(X_train),batchSize)):
                inp, out = getData(i,X_train,y_train,batchSize=batchSize)
                #print inp.shape
                avg_training_cost+=train_fn(inp,out)
                trainIterations+=1
            print "\tIN-SAMPLE ERROR = {}".format(avg_training_cost/trainIterations)
            print "\tTesting phase..."
            testIterations = 0
            avg_test_cost = 0.0
            avg_test_acc = 0.0
            for i in tqdm.tqdm(xrange(len(X_test))):
                inp, out = getData(i,X_test,y_test,batchSize=1)
                cost, acc, pred,pp  = test_fn(inp,out)
                #print cost, acc, pred
                #print numpy.argmax(inp[0],axis=1)
                avg_test_cost += cost
                avg_test_acc += acc
                testIterations+=1
            print "\tOUT OF SAMPLE ERROR = {}".format(avg_test_cost/testIterations)
            print "\tOUT OF SAMPLE ACCURACY = {}".format(avg_test_acc/testIterations)
            print "_______________________________"
            print "###### SAVING THE MODEL ######"
            numpy.savez(modelName+".npz", *lasagne.layers.get_all_param_values(model))
        print "Saving final model..."
        numpy.savez(modelName+"_fold"+str(kf_index)+".npz", *lasagne.layers.get_all_param_values(model))
        print "Done."
        kf_index+=1
    
    
    