import csv
import os
import numpy
import random
import tqdm
import cPickle
import theano
import lasagne

# Theano float policy
theano.config.floatX = 'float32'

# Reproducibility
random.seed(0)
numpy.random.seed(0)

class CharacterOrientedTextBasedEnsambleClassifier(object):
    def __init__(self):
        path = os.path.dirname(os.path.realpath(__file__))
        modelNames = [path+"/textBasedClassifier_characterOriented_fold0.npz",
                  path+"/textBasedClassifier_characterOriented_fold1.npz",
                  path+"/textBasedClassifier_characterOriented_fold2.npz",
                  path+"/textBasedClassifier_characterOriented_fold3.npz",
                  path+"/textBasedClassifier_characterOriented_fold4.npz"]
        self.models = []
        for name in modelNames:
            self.models.append(CharacterOrientedTextBasedClassifier(name))
        
        
    def _getEnsamblePrediction(self,s,ensambleList):
        result = None
        count = 0
        for i in ensambleList:
            if(result is None):
                result = numpy.array(i.probabilityRumor(s))
            else:
                result += numpy.array(i.probabilityRumor(s))
            count+=1
        return result/count
    
    def probabilityRumor(self,s):
        return self._getEnsamblePrediction(s,self.models)
    
    def isRumor(self,s,withConfidence=True):
        prediction = self.probabilityRumor(s)
        p = numpy.argmax(prediction)
        confidence = prediction[p]
        return p,confidence
        
class CharacterOrientedTextBasedClassifier(object):
    def __init__(self,modelFilename):
        self.n_terms = 241
        self.dictionary = "abcdefghijklmnopqrstuvwxyz@# "
        self.dictionaryToIndex = {t:i+2 for i,t in enumerate(self.dictionary)}
        self.dictionaryLength = len(self.dictionary)+2
        input_var = theano.tensor.tensor3(name="input", dtype=theano.config.floatX)
        self.model = self._buildModel(input_var)
        parameters = numpy.load(modelFilename)
        param_values = [parameters['arr_%d' % i] for i in range(len(parameters.files))]
        lasagne.layers.set_all_param_values(self.model, param_values)
        
        prediction = lasagne.layers.get_output(self.model, deterministic=True)
        output = theano.tensor.argmax(prediction, axis=1)
        confidence = prediction[0,output[0]]
        self.getConfidence = theano.function([input_var], [output,confidence])
        self.getPrediction = theano.function([input_var], [prediction])

    def _preprocess(self,s):
        #intRepresentation = [ord(letter)+2 if ord(letter)+2<=self.n_terms else 1 for letter in s]
        tmp = []
        for letter in s:
            letter = letter.lower()
            if(letter in set([i for i in self.dictionary])):
                tmp.append(letter)
        s = "".join(tmp)
        intRepresentation = [self.dictionaryToIndex[letter] if letter in self.dictionaryToIndex else 1 for letter in s]
        matrix = [[1 if j==i else 0 for j in xrange(self.dictionaryLength)] for i in intRepresentation]
        #matrix.append([1 if j==0 else 0 for j in xrange(self.n_terms)])
        matrix=numpy.array(matrix,dtype=numpy.float32)
        #print matrix.tolist()
        return matrix
    
    def probabilityRumor(self,s):
        prediction = self.getPrediction([self._preprocess(s)])
        prediction = prediction[0][0]
        return prediction
    
    def _buildModel(self,input_var):
        net = lasagne.layers.InputLayer(input_var=input_var, shape=(None, None, self.dictionaryLength))
        net = lasagne.layers.LSTMLayer(
                net, 32, grad_clipping=50,
                nonlinearity=lasagne.nonlinearities.tanh,
                only_return_final=True)
        net = lasagne.layers.DenseLayer(net, 
        num_units=2, W = lasagne.init.HeNormal(), 
        nonlinearity = lasagne.nonlinearities.softmax)
        return net

   
if __name__=="__main__":
    fd = open("rawDatasets/NodeXL/users-last-10-tweetsALL.csv","rb")
    csvReader = csv.reader(fd,delimiter=';')
    data = []
    header = csvReader.next()
    header.append("isRumor")
    header.append("probabilityItIsRumor")
    for i in csvReader:
        data.append(i)
    
    for i in tqdm.tqdm(data):
        try:
            pred = model.probabilityRumor(i[16])
            i.append(numpy.argmax(pred))
            i.append(pred[1])
        except:
            i.append(None)
            i.append(None)
    writer = open("rawDatasets/NodeXL/users-last-10-tweetsALL_predicted.csv","wb")
    csvWriter = csv.writer(writer,delimiter=';')
    csvWriter.writerow(header)
    for i in data:
        csvWriter.writerow(i)
    writer.flush()
    writer.close()