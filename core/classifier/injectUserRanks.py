import csv
fd = open("rawDatasets/NodeXL/users-last-10-tweetsALL_predicted_withUserRank.csv","rb")
csvReader = csv.reader(fd,delimiter=';')
data = []
header = csvReader.next()
for i in csvReader:
    data.append(i)
userDictionary = {}
for i in data:
    if(i[0] not in userDictionary):
        userDictionary[i[0]]=i[-1]
fd = open("rawDatasets/NodeXL/Crawled-Dataset-Final-02Nov_predicted.csv","rb")
csvReader = csv.reader(fd,delimiter=';')
data = []
header = csvReader.next()
header.append("UserRankVertex1")
header.append("UserRankVertex2")
for i in csvReader:
    data.append(i)
c=0
for i in data:
    if(i[0] in userDictionary):
        i.append(userDictionary[i[0]])
    else:
        i.append(None)
        c+=1
    if(i[1] in userDictionary):
        i.append(userDictionary[i[1]])
    else:
        i.append(None)
        c+=1
print c
fd = open("rawDatasets/NodeXL/Crawled-Dataset-Final-02Nov_predicted_withUserRank.csv","wb")
csvWriter = csv.writer(fd,delimiter=';')
csvWriter.writerow(header)
for i in data:
    csvWriter.writerow(i)
fd.flush()
fd.close()