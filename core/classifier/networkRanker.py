import csv
import numpy
import sklearn 
import sklearn.linear_model

def featuresEngineering(userRanksVector):
    maximum = numpy.max(userRanksVector)
    minimum = numpy.min(userRanksVector)
    mean = numpy.mean(userRanksVector)
    std = numpy.std(userRanksVector)
    return mean,std,maximum,minimum
    
def buildDataset(userRanksDictionary,incoming,outcoming):
    X=[]
    y=[]
    names = []
    for user in userRanksDictionary:
        names.append(user)
        tmpX = []
        y.append(userRanksDictionary[user])
        if(user in incoming):
            for i in incoming[user]:
                tmpX.append(userRanksDictionary[i])
        if(user in outcoming):
            for i in outcoming[user]:
                tmpX.append(userRanksDictionary[i])
        tmpFeatures=featuresEngineering(tmpX)
        X.append(tmpFeatures)
    X = numpy.array(X)
    y = numpy.array(y)
    return X,y,names

if __name__=="__main__":
    testSamples = 1000
    fd = open("rawDatasets/NodeXL/Crawled-Dataset-Final-02Nov_predicted_withUserRank.csv","rb")
    csvReader = csv.reader(fd,delimiter=';')
    data = []
    header = csvReader.next()
    for i in csvReader:
        if(i[0]!=i[1]):
            data.append(i)
    outcoming = {}
    for i in data:
        if(i[0] not in outcoming):
            outcoming[i[0]] = []
        outcoming[i[0]].append(i[1])
    incoming = {}
    for i in data:
        if(i[1] not in incoming):
            incoming[i[1]] = []
        incoming[i[1]].append(i[0])
    userRanks = {}
    for example in data:
        if(example[-2]!=""):
            userRanks[example[0]] = float(example[-2])
        else:
            userRanks[example[0]] = 0
        if(example[-1]!=""):
            userRanks[example[1]] = float(example[-1])
        else:
            userRanks[example[1]] = 0
    X,y,names = buildDataset(userRanks,incoming,outcoming)
    y_yo = y
    fittedModel = None
    for i in xrange(6):
        X,y,names = buildDataset(userRanks,incoming,outcoming)
        if(fittedModel is None):
            model = sklearn.linear_model.LinearRegression(n_jobs=4)
            fittedModel = model.fit(X[:len(X)-testSamples],y[:len(X)-testSamples])
        print fittedModel.score(X[len(X)-testSamples:],y_yo[len(X)-testSamples:])
        newScores = fittedModel.predict(X)
        newScores = (0.005*numpy.array(newScores)+0.995*numpy.array(y))
        newUserRanks = {n:s for n,s in zip(names,newScores)}
        userRanks = newUserRanks
    header.append("NetworkRankerRefined1")
    header.append("NetworkRankerRefined2")
    for i in data:
        if(i[0] in userRanks):
            i.append(userRanks[i[0]])
        else:
            i.append(None)
        if(i[1] in userRanks):
            i.append(userRanks[i[1]])
        else:
            i.append(None)
    fd = open("rawDatasets/NodeXL/Crawled-Dataset-Final-02Nov_predicted_withUserRank_andNetworkRanks.csv","wb")
    writer = csv.writer(fd,delimiter=';')
    writer.writerow(header)
    for i in data:
        writer.writerow(i)
    fd.flush()
    fd.close()
    