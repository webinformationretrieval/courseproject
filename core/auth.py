import webbrowser

import tweepy
import time
from conf import parameters

auth = tweepy.OAuthHandler(parameters.twitter_key, parameters.twitter_secret)

# Open authorization URL in browser
webbrowser.open(auth.get_authorization_url())
time.sleep(3)

# Ask user for verifier pin
pin = raw_input('Verification pin number from twitter.com: ').strip()

# Get access token
token = auth.get_access_token(verifier=pin)

# Give user the access token
print 'Access token:'
print '  Key: %s' % token[0]
print '  Secret: %s' % token[1]
print 'Change your user parameters to match these in core/conf/uparameters.py'