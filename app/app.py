import json
from core.invertedIndex.indexer import Searcher
from core.topicClassifier.classifierNB import TopicClassifier
from core.classifier.models import CharacterOrientedTextBasedEnsambleClassifier
from core import iocsv, crawler, preprocessor
from flask import Flask, make_response, render_template, request, jsonify

app = Flask(__name__)

tweetFile = "tweets/tweets.csv"
searcher = Searcher()

@app.route("/")
def main():
    return render_template('index.html')

@app.route('/search', methods=['POST'])
def get_tweets():
    tweets = crawler.search_by_term(request.form["term"])
    processedTweetsTuple = process_tweets(tweets)
    processedTweets = processedTweetsTuple[0]
    keywords = processedTweetsTuple[1]
    values = [tweet.to_json() for tweet in processedTweets[:10]]
    response = make_response()
    response.data = json.dumps({'tweets': values,
                                'health': "Classified as health related:  %s/%s" % (str(len(processedTweets)), str(len(tweets))),
                                'keywords': keywords})
    return response

def process_tweets(tweets):
    topicClf = TopicClassifier()
    rumorClf = CharacterOrientedTextBasedEnsambleClassifier()
    for tweet in tweets:
        tweet.normalizedText = preprocessor.preprocess(tweet.text)

    tweets2 = []
    for tweet in tweets:
        norm = [x.normalizedText for x in tweets2]
        if tweet.normalizedText not in norm:
            tweets2.append(tweet)

    for tweet in tweets2:
        tweet.health = topicClf.is_health_related(tweet.text)
        tweet.rumor = rumorClf.isRumor(tweet.text)[0]
        tweet.rumorProb = float(rumorClf.probabilityRumor(tweet.text)[0])
        print(tweet.rumorProb)

    filteredTweets = [x for x in tweets2 if topicClf.is_health_related(x.text)]
    filteredTweets.sort(key=lambda x: x.rumorProb, reverse=True)

    indexTuple = searcher.create_index_dict(x.normalizedText for x in filteredTweets)
    iocsv.write_file(tweetFile, filteredTweets)

    return filteredTweets, indexTuple[1]


@app.route('/filter', methods=['POST'])
def filter_tweets():
    term = request.form["term"]
    indexes = searcher.search(term)
    tweets = iocsv.read_tweets_by_index(tweetFile, indexes)

    tweets.sort(key=lambda x: x.rumorProb, reverse=True)
    if (len(tweets)>10):
        tweets = tweets[:10]
    response = make_response()
    values = [tweet.to_json() for tweet in tweets]
    response.data = json.dumps({'tweets': values})
    return response

@app.route('/all', methods=['POST'])
def all_tweets():
    tweets = iocsv.read_tweets(tweetFile)
    tweets.sort(key=lambda x: x.rumorProb, reverse=True)
    if (len(tweets)>10):
        tweets = tweets[:10]
    response = make_response()
    values = [tweet.to_json() for tweet in tweets]
    response.data = json.dumps({'tweets': values})
    return response

if __name__ == "__main__":
    app.run()
