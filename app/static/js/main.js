(function($) {
    $.fn.filter = function(word) {
        $("#table tr").remove();
        var buttons = $('.filter');
        console.log(buttons.length);

        $.ajax({
            type: "POST",
            url: "/filter",
            data: {term: word},
            success: function(data) {
                console.log(data);
                var json = $.parseJSON(data);
                var tweets = json.tweets;
                if (tweets.length > 0) {
                    var table = $('#table');
                    var head = $("<tr>");
                         head.append($("<td>"+"Tweet"+"</td>"))
                            .append($("<td width=\"15%\">"+"Rumor %"+"</td>"))
                          head.css("border","1px solid #000");
                          table.append(head)
                    tweets.forEach(function(obj) {
                        var row = $("<tr>");
                        row.css("border","1px solid #000");
                        var r = Math.round(obj["rumorProbRaw"]*255);
                        var b = 255-r;
                        row.css("background-color", "rgba("+r+", 0, "+b+", 0.5)");

                        row.append($("<td>"+obj["text"]+"</td>"))
                            .append($("<td>"+obj["rumorProb"]+"</td>"))

                        table.append(row)
                    });
                }
             },
        });
    };

    $.fn.all = function() {
        $("#table tr").remove();
        var buttons = $('.filter');
        console.log(buttons.length);

        $.ajax({
            type: "POST",
            url: "/all",
            data: {},
            success: function(data) {
                console.log(data);
                var json = $.parseJSON(data);
                var tweets = json.tweets;
                if (tweets.length > 0) {
                    var table = $('#table');
                    var head = $("<tr>");
                         head.append($("<td>"+"Tweet"+"</td>"))
                            .append($("<td width=\"15%\">"+"Rumor %"+"</td>"))
                          head.css("border","1px solid #000");
                          table.append(head)
                    tweets.forEach(function(obj) {
                        var row = $("<tr>");
                        row.css("border","1px solid #000");
                        var r = Math.round(obj["rumorProbRaw"]*255);
                        var b = 255-r;
                        row.css("background-color", "rgba("+r+", 0, "+b+", 0.5)");

                        row.append($("<td>"+obj["text"]+"</td>"))
                            .append($("<td>"+obj["rumorProb"]+"</td>"))

                        table.append(row)
                    });
                }
             },
        });
    };
})(jQuery);


$(document).ready( function() {
	$('#search-btn').click(function(){
	    $("#table tr").remove();
	    $('#label-health').text("");
	    $('#buttons button').remove();

		$.ajax({
            type: "POST",
            url: "/search",
            data: {term: $('#search-box').val()},
            success: function(data) {
                console.log(data);
                var json = $.parseJSON(data);
                var tweets = json.tweets;
                $('#label-health').text(json.health);
                if (tweets.length > 0) {
                    var table = $('#table');
                    var head = $("<tr>");
                         head.append($("<td>"+"Tweet"+"</td>"))
                            .append($("<td width=\"15%\">"+"Rumor %"+"</td>"))
                          head.css("border","1px solid #000");
                          table.append(head)
                    tweets.forEach(function(obj) {
                        var row = $("<tr>");
                        row.css("border","1px solid #000");
                        var r = Math.round(obj["rumorProbRaw"]*255);
                        var b = 255-r;
                        row.css("background-color", "rgba("+r+", 0, "+b+", 0.5)");

                        row.append($("<td>"+obj["text"]+"</td>"))
                            .append($("<td>"+obj["rumorProb"]+"</td>"))

                        table.append(row)
                    });
                }

                var keywords=json.keywords;
                if (keywords.length > 0 && tweets.length > 2) {
                    var buttons = $('#buttons');
                    var primary='<button type="button" class="btn filter btn-info" onclick="$(this).all()">All</button>';
                    buttons.append(primary)
                    keywords.forEach(function(word) {
                       var button='<button type="button" class="btn filter btn-info" onclick="$(this).filter(\''+word+'\');">'+word+'</button>';

                        buttons.append(button)
                    });
                }
             },
        });
	});
});

