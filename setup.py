try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'Social networks rumor analytics for Medical tweets',
    'author': 'The Group - 4',
    'url': 'https://bitbucket.org/webinformationretrieval/',
    'download_url': '',
    'author_email': '',
    'version': '0.1',
    'install_requires': ['flask'],
    'packages': ['core', 'server', 'conf'],
    'scripts': [],
    'name': 'trumor'
}

setup(**config)
