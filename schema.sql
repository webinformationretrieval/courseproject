DROP TABLE IF EXISTS tweet;
CREATE TABLE  tweet (
    id integer primary key autoincrement,
    'tweet_id' integer not null,
    'author_id' integer not null,
    'text' text default null,
    'rawtext' text not null,
    'favorite_count' int not null,
    'retweet_count' int not null,
    'lang' varchar(3) not null,
    'timestamp' datetime not null,
    'health' integer null,
    'rumor' integer null
    );

DROP TABLE IF EXISTS author;
CREATE TABLE  author (
    id integer primary key autoincrement,
    'screen_name' text not null,
    'verified' integer not null,
    'statuses_count' integer not null,
    'friends_count' integer not null,
    'lang' varchar(3) not null,
    'created_at' datetime not null,
    'health' integer null,
    'rumor' integer null
    );
