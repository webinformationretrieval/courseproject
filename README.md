# Trumor
Social network rumor analytics for Medical tweets

## Preferred IDE
[PyCharm](https://www.jetbrains.com/pycharm/) - JetBrains

## Fetching your project
Create a new project folder:
```
mkdir ir
```

Get the project:
```
cd ir
git init
git remote add origin https://[username]@bitbucket.org/webinformationretrieval/courseproject
git pull origin master
```

## Setting up the project

Install virtualenv via pip:
```
pip install virtualenv
```
In your project root, create a virtual environment:
```
virtualenv -p /usr/bin/python2.7 venv
```

Activate the virtual environment:
```
source ./activate
```
> You should notice that the prompt now starts with `(venv)` to indicate that the environment is active.

Install the dependencies from `requirements.txt` file:
```
pip install -r requirements.txt
```

To install more dependencies:
```
./pips [dependency]
```

For the rumor classifier Theano and Lasagne should be installed separately by running:
```
./install-theano.sh
```

*Wherever in this document a command has to be run, it is assumed that the environment is activated.*

When done, deactivate the virtual environment (or just close the console):
```
deactivate
```

## Getting your twitter access token and secret

An application on Twitter has already been created with the keys in `core/conf/parameters`. You need to generate your own access token and access secret to be able to use the application in command line mode.
To generate the access token, run `core/auth.py`.

```
python core/auth.py
```

Your default web browser will open and you will be prompted to authorise the application. Now you get a PIN. Enter the PIN in the console and hit Enter.

Copy the access key and access secret to `core/conf/uparameters.py`. This file is git-ignored so no one from the team will see it.

## Pre Processor

Pre-processor uses stop-words from nltk stopword library. So in order to use it nltk corpus should be downloaded by running:

```
nltk.download()
```

## Web App

Run Flask web app to live query Twitter and get analyzed results. After each query it takes approx 1 min to process and classify the tweets queried.

```
./app/app.py
```

##Classifier, User Ranker and Network Ranker

To train the recurrent neural network use the command:

```
python -m core.classifier.train
```

This will run a 5-fold cross validation and produce 5 classifiers whose parameters will be saved as .npz files in the main path of this project, move those files in core/classifier.

Running:

```
python -m core.classifier.models
```

Will add to the file “rawDatasets/NodeXL/users-last-10-tweetsALL.csv” a new column representing the probabilities of the tweet being a rumor and save it as “rawDatasets/NodeXL/users-last-10-tweetsALL_predicted.csv".

Running:

```
python -m core.classifier.userRanker
```

will update the "rawDatasets/NodeXL/users-last-10-tweetsALL_predicted.csv" file adding a new column for the user rank and saving it as "rawDatasets/NodeXL/users-last-10-tweetsALL_predicted_withUserRank.csv"

Finally:

```
python -m core.classifier.networkRanker 
```

will update “rawDatasets/NodeXL/users-last-10-tweetsALL_predicted.csv” with the refined user ranks and save it as "rawDatasets/NodeXL/Crawled-Dataset-Final-02Nov_predicted_withUserRank_andNetworkRanks.csv"

